//@ts-check
const type = require('./@type.js')
const lib_os = require('os')
const lib_tds = require('tedious')
const lib_conv = require('viva-convert')

exports.options_beautify = options_beautify
exports.options_to_tds = options_to_tds
exports.open = open
exports.close = close

/**
 * @param {type.connection_option} option
 * @return {type.connection_option}
 */
function options_beautify(option) {
    /** @type {type.connection_option} */
    let connection_option = (!lib_conv.isAbsent(option)
        ?
        {
            instance: lib_conv.toString(option.instance, ''),
            login: lib_conv.toString(option.login, ''),
            password: lib_conv.toString(option.password, '')
        }
        :
        {
            instance: '',
            login: '',
            password: ''
        }
    )

    let additional = (lib_conv.isAbsent(option) || lib_conv.isAbsent(option.additional) ? {} : option.additional)

    connection_option.additional = {
        database: lib_conv.toString(additional.database, 'tempdb'),
        app_name: lib_conv.toString(additional.app_name, 'vmst-driver'),
        use_utc: lib_conv.toBool(additional.use_utc, true),
        encrypt_connection: lib_conv.toBool(additional.encrypt_connection, false),
        execution_timeout: lib_conv.toInt(additional.execution_timeout, 0),
        connection_timeout: lib_conv.toInt(additional.connection_timeout, 15000),
    }
    if (connection_option.additional.execution_timeout < 0) connection_option.additional.execution_timeout = 0
    if (connection_option.additional.connection_timeout < 0) connection_option.additional.connection_timeout = 15000

    return connection_option
}

/**
 * @param {type.connection_option} option
 * @return {lib_tds.ConnectionConfig}
 */
function options_to_tds(option) {
    if (lib_conv.isAbsent(option)) return undefined

    let sql_authentication = (!lib_conv.isEmpty(option.login) && !lib_conv.isEmpty(option.password))
    let server = option.instance.split("\\").join("/").split("/")

    /** @type {lib_tds.ConnectionConfig} */
    return {
        server: server[0],
        authentication: {
            type: (sql_authentication ? 'default' : 'ntlm'),
            options: {
                userName: (sql_authentication ? option.login : lib_os.userInfo().username),
                password: (sql_authentication ? option.password : ''),
                domain: (sql_authentication ? '' : lib_os.hostname().toUpperCase()),
            }
        },
        options: {
            appName: option.additional.app_name,
            connectTimeout: option.additional.connection_timeout,
            requestTimeout: option.additional.execution_timeout,
            database: option.additional.database,
            encrypt: option.additional.encrypt_connection,
            instanceName: (server.length > 1 ? server.splice(1, server.length).join("/") : undefined),
            useColumnNames: false,
            useUTC: option.additional.use_utc,
            trustServerCertificate: false
        }
    }
}

/**
 * @callback callback_connect
 * @param {Error} error
 * @param {type.connection} connection
 *//**
 * @param {lib_tds.ConnectionConfig} connection_option_tds
 * @param {type.exec_result_end} result_funded
 * @param {callback_connect} callback
 */
function open(connection_option_tds, result_funded, callback) {
    let has_internal_error = false 

    if (lib_conv.isAbsent(connection_option_tds)) {
        callback(new Error('connection not initialized'), undefined)
        return
    }
    
    /** @type {type.connection} */ let connection = {
        tds_connection: new lib_tds.Connection(connection_option_tds),
        current_state: {allow_message: true, table_index: -1, query_index: 0}
    }

    connection.tds_connection.on('error', (error) => {
        has_internal_error = true
        close(connection)
        callback(error, undefined)
    })

    connection.tds_connection.on('connect', (error) => {
        if (has_internal_error) {
            return
        }

        connection.tds_connection.on('databaseChange', (database) => {
            result_funded.database = database
        })

        connection.tds_connection.on('infoMessage', (info_message) => {
            if (connection.current_state.allow_message !== true) return
            result_funded.message_list.push({
                type: 'info',
                query_index: connection.current_state.query_index,
                message: info_message.message,
                proc_name: info_message.procName,
                line: info_message.lineNumber
            })
        })

        connection.tds_connection.on('errorMessage', (err_message) => {
            result_funded.message_list.push({
                type: 'error',
                query_index: connection.current_state.query_index,
                message: err_message.message,
                // @ts-ignore
                proc_name: err_message.procName,
                // @ts-ignore
                line: err_message.lineNumber
            })
        })

        // @ts-ignore
        if (!lib_conv.isEmpty(error) && error.code === 'EINSTLOOKUP') {
            return
        }

        if (!lib_conv.isEmpty(error)) {
            callback(error, undefined)
        } else {
            callback(undefined, connection)
        }
    })
}

/**
 * @param {type.connection} connection
 */
function close(connection) {
    if (lib_conv.isAbsent(connection)) return
    if (!lib_conv.isAbsent(connection.tds_connection)) {
        connection.tds_connection.removeAllListeners('connect')
        connection.tds_connection.removeAllListeners('databaseChange')
        connection.tds_connection.removeAllListeners('infoMessage')
        connection.tds_connection.removeAllListeners('errorMessage')
        connection.tds_connection.close()
        connection.tds_connection = undefined
    }
    connection = undefined
}